module gitlab.com/etke.cc/int/ansible-wg-sync

go 1.21

require (
	github.com/adrg/xdg v0.4.0
	gitlab.com/etke.cc/go/ansible v0.0.0-20231122192730-9f20982f38ed
	gopkg.in/yaml.v3 v3.0.1
)

require (
	golang.org/x/exp v0.0.0-20231110203233-9a3e6036ecaa // indirect
	golang.org/x/sys v0.14.0 // indirect
)
